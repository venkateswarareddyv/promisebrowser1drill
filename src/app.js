import { createDomElements } from "./func";

fetch("https://jsonplaceholder.typicode.com/users")
.then(response=> response.json()
).then(data=>{
    let data1=data.map((eachUser)=>{
        return fetch(`https://jsonplaceholder.typicode.com/todos?userId=${eachUser.id}`)
    }) 
    return Promise.all([data,Promise.all(data1)]);
    
}).then(([data,data1]) => {
    let temp=(data1.map(each => {
        return each.json()
    }))
    return Promise.all([data,Promise.all(temp)])
}).then(([data,data1])=>{
    let todosList=data1.map((todo)=>{
        return todo.splice(0,7)
    })
    toGetUserTodos(data,todosList)
})

function toGetUserTodos(users,todos){
    let totalArr = []
    let onlyUser=users.map(user=>user.name)
    let toDoTitles=[]
    todos.map((todoIndividual)=>{
        let array=[]
        todoIndividual.map((todo)=>{
            array.push(todo.title)
        })
        toDoTitles.push(array)
    })
    console.log(toDoTitles)
    onlyUser.map((userName ,index)=> {
        totalArr.push({[userName]:toDoTitles[index]})
    })
    // console.log(totalArr)
    createDomElements(totalArr)
}




