let container=document.getElementById("mainContainer");
function createDomElements(data){
    
    data.map((object)=>{
        let h1=document.createElement("h1");
        h1.innerHTML= Object.keys(object);
        let todo=Object.values(object)[0];
        container.appendChild(h1);
        let div=document.createElement("div")

        todo.map((array,index)=>{
            div.innerHTML +=`<li><input type="checkbox">${array}</li>`
        })
        container.appendChild(div);

    })
}

export {createDomElements};

